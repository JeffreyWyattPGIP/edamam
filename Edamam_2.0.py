# This script grabs 100 recipes related to an input search term and
# randomly selects web links for up to five recipes from Edamam.com

import json
from random import choice
from requests import get

# API and credentials
api_url = "https://api.edamam.com/search"

app_id = 'eeec6975'
app_key = '0e5ddae4b3f7617dd874fdb33023f8c2'

# Request search term input
query_term = input('Enter a recipe search term: ')

# Query parameters
params = {
    "q": query_term,
    "app_id": app_id,
    "app_key": app_key,
    "from": 0,
    "to": 100,
    }

# Request JSON data
response_object = get(api_url, params=params)

# Convert to dictionary
data = response_object.json()

# Convert to formatted string
json_formatted_data_object = json.dumps(data, indent = 2)

# Global recipe list
recipe_links = []

# Iterate through recipes for URLs, add to recipe_links
for index, hit in enumerate(data["hits"]):
    recipe = hit["recipe"]
    recipe_links.append(recipe["url"])

# Global list
random_recipe_links = []

# Create a list of up to 5 web links for recipes
# Counter to prevent infinite loop for limited recipe count at Edamam
max_loop_counter = 0
# While there are less than 5 links in list
while len(random_recipe_links) < 5:
    # Break to prevent infinite loop
    if max_loop_counter == 5:
        break
    # Add items to list
    try:
        random_recipe = choice(recipe_links)
        # Avoid redundant links
        if random_recipe not in random_recipe_links:
            random_recipe_links.append(random_recipe)
    except:
        # If search term is invalid, display error and quit
        print('Invalid search term')
        exit()
    max_loop_counter += 1

# Display results
for recipe in random_recipe_links:
    print(recipe)
